.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:AdvancedMultiBlock:

Advanced: MultiBlock
####################

Introduction
============

This tutorial covers multiblock processing. 

Most examples assume that the user starts with a new model. To start over, 
go to the menu item Edit → Reset Session, and then re-open your data.

Data is opened by going to File → Open. Example data files can be found in 
the Examples directory, located in the upper left of the Open dialog.

Block commands onscreen menu
============================

bake.e is a dataset that simulates the heating of a cone in a test
facility. It has numerous blocks. **T** (for temperature) is the primary
variable of interest.

-  Open the dataset bake.e.
-  **Apply**.
-  Your dataset is now colored by block.
-  Change **vtkBlockColors** to **T**.
-  Your dataset is now colored by temperature.
-  Play forward in time, and look into the simulation.
-  Be sure to rescale to data range.
-  Change color back to **vtkBlockColors**.

Notice that this model has 6 sides, and there are 12 annotation colors.
Thus, they repeat. Lets make them random.

-  In the **Properties** tab, click **Advanced**.
-  Slide down towards the bottom, and find **Block Colors Distinct Values**, and change it to **11**.
-  Right click on a side (a block). Notice that the bock name and ID are shown.
-  Right click on a side. Change the block's color and opacity.
-  Right click on a side. Hide, then un-hide the block.

.. figure:: ../../images/Advanced_multiblock_1.png
   :width: 1000px

Don't load some blocks
======================

You can easily change which blocks you load. This is done through
selections. Here is an example.

-  Open the dataset bake.e.
-  **Apply**.
-  Your dataset is now colored by block.
-  Choose the **Select Block** icon (or hit the **b** key), then rubber band
   select the blocks on the outside edge of bake. You have selected
   numerous blocks.
-  In the **Properties** tab, slide down to the section named Blocks.
-  Click **Uncheck Selected Blocks**.
-  **Apply**.

.. figure:: ../../images/Advanced_multiblock_2.png
   :width: 1000px

Load the same dataset twice
===========================

-  Open the dataset can.ex2.
-  Only turn on block ID **2**.
-  **Apply**.
-  Open the dataset can.ex2. 
-  Only turn on block ID **1**. 
-  **Apply**.
-  Change the timestep to 1.
-  Set **Coloring** to **ACCL**.
-  Play

.. figure:: ../../images/Advanced_multiblock_3.png
   :width: 1000px

Extract Block and Extract Selection
===================================

Two filters that can also be used to partition your data are the **Extract
Block** and **Extract Selection** filters. Instead of reading your data in
twice, read it in once and Extract Block twice.

MultiBlock inspector
=====================

The **View → MultiBlock inspector** can also be used to toggle visibility, and
change colors and opacities of selected blocks.
